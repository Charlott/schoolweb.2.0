from django.urls import path
from .views import home, nosotros, comunidad, admision, documento, inscripcion

urlpatterns = [
    path('', home, name="home"),
    path('nosotros/', nosotros, name="nosotros"),
    path('comunidad/', comunidad, name="comunidad"),
    path('admision/', admision, name="admision"),
    path('documento/', documento, name="documento"),
    path('inscripcion/', inscripcion, name="inscripcion"),
]
