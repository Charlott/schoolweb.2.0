from django.shortcuts import render

# Create your views here.

def home(request):
    return render(request, 'core/home.html')

def nosotros(request):
    return render(request, 'core/nosotros.html')

def comunidad(request):
    return render(request, 'core/comunidad.html')

def admision(request):
    return render(request, 'core/admision.html')

def documento(request):
    return render(request, 'core/documento.html')

def inscripcion(request):
    return render(request, 'core/inscripcion.html')     
     
