from django.db import models

# Create your models here.

class Alumno(models.Model):
	rut_alumno = models.CharField(max_length=10, primary_key=True)
	nombre_alumno = models.CharField(max_length=20)
	apellido_alumno = models.CharField(max_length=20)
	SEXO = (
		('Femenino','Femenino'),
		('Maculino', 'Masculino')
		)
	sexo = models.CharField(max_length=10, choices=SEXO, default= 'Femenino')
	fechaNacimiento = models.DateField(null=False, blank=False)

	def __str__(self):
	 return '{}'.format(self.rut_alumno)