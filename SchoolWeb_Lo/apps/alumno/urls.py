from django.conf.urls import url
from apps.alumno.views import index_alumno

urlpatterns = [
    url(r'^$', index_alumno)
]