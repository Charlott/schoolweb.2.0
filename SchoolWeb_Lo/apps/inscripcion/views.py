from django.shortcuts import render, redirect
from django.http import HttpResponse
from apps.inscripcion.forms import InscripcionForm
from apps.inscripcion.models import Inscripcion
from datetime import datetime
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy

# Create your views here.

def confirmacion(request):
    return render(request,'inscripcion/confirmacion.html')

def inscripcion_view(request):
    if request.method == 'POST':
        form = InscripcionForm(request.POST)
        if form.is_valid():
            form.save()
        return redirect(request,'inscripcion:confirmacion')
    else:
        form = InscripcionForm()
        return render(request, 'inscripcion/inscripcion.html', {'form':form})      

def inscripcion_delete(request, id_inscripcion):
    inscripcion= Inscripcion.objects.get(id_inscripcion)
    if request.method == 'POST':
        inscripcion.delete()
        return redirect(request,'inscripcion:inscripcion_listar')
    return render(request, 'inscripcion/inscripcion_delete.html', {'inscripcion':inscripcion})            

class inscripcionList (ListView):
    model = Inscripcion
    template_name = 'inscripcion/inscripcion_listar.html'

#class inscripcionCreate (CreateView):
    #model = Inscripcion
    #form_class = InscripcionForm
    #template_name = 'inscripcion/inscripcion.html'
    #success_url = reverse_lazy('inscripcion:inscripcion_listar')

class inscripcionCreate (CreateView):
    model = Inscripcion
    form_class = InscripcionForm
    template_name = 'inscripcion/inscripcion.html'
    success_url = reverse_lazy('inscripcion:inscripcion_listar')
    
class inscripcionUpdate (UpdateView):
    model = Inscripcion
    form_class = InscripcionForm
    template_name = 'inscripcion/inscripcion.html'
    success_url = reverse_lazy('inscripcion:inscripcion_listar')
    
class inscripcionDelete (DeleteView):
    model = Inscripcion
    template_name = 'inscripcion/inscripcion_delete.html'
    success_url = reverse_lazy('inscripcion:inscripcion_listar')