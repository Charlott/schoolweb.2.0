from django.conf.urls import url, include
from apps.inscripcion.views import confirmacion, inscripcionList, inscripcionCreate, inscripcionUpdate, inscripcionDelete
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^$', confirmacion, name='confirmacion'),
    url(r'^nuevo/$', login_required(inscripcionCreate.as_view()), name='inscripcion'),
    url(r'^listar/$', inscripcionList.as_view(), name='inscripcion_listar'),
    url(r'^editar/(?P<pk>\d+)/$', inscripcionUpdate.as_view(), name='inscripcion_editar'),
    url(r'^eliminar/(?P<pk>\d+)/$', inscripcionDelete.as_view(), name='inscripcion_eliminar'),
]