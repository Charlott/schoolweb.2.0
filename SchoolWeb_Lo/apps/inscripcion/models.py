from django.db import models
from datetime import datetime
from apps.alumno.models import Alumno

# Create your models here.

class Inscripcion(models.Model):
  fechaInscrip = models.DateTimeField(default=datetime.now, null=False, blank=False)
  rut = models.ForeignKey(Alumno, null=True, blank=False, on_delete=models.CASCADE)
  NOMBRE=(
     ('Pintura','Pintura'),
     ('Musica','Musica'),
     ('Contabilidad','Contabilidad'),
     ('Diseño Web','Diseño Web'),
     ('Escritura','Escritura'),               
   )
  nombre = models.CharField(max_length=15, choices=NOMBRE, default='Pintura')

  def __str__(self):
     return '{} {}'.format(self.rut, self.nombre)
