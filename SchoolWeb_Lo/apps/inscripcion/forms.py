from django import forms
from apps.inscripcion.models import Inscripcion


class InscripcionForm(forms.ModelForm):

    class Meta: 
        model = Inscripcion
        
        fields = [
            'fechaInscrip',
            'rut',
            'nombre',
        ]

        labels ={
            'fechaInscrip': 'Fecha',
            'rut': 'Alumno',
            'nombre': 'Nombre del Curso',  
        }

        widgets = {
            'fechaInscrip': forms.DateTimeInput(attrs={'class':'form-control'}),
            'rut': forms.Select(attrs={'class':'form-control'}),
            'nombre': forms.Select(attrs={'class':'form-control'}),
        }