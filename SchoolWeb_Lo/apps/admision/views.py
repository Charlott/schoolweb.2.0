from django.shortcuts import render
from django.http import HttpResponse
from apps.admision.form import AdmisionForm
from apps.admision.models import Admision
from datetime import datetime
from django.views.generic import CreateView, ListView
from django.urls import reverse_lazy

# Create your views here.

def admision(request):
    return HttpResponse('admision/admision.html')

class admisionCreate (CreateView):
    model = Admision
    form_class = AdmisionForm
    template_name = 'admision/admision.html'
    success_url = reverse_lazy('admision_listar')  

class admisionList (ListView):
    model = Admision
    template_name = 'admision/admision_listar.html'    