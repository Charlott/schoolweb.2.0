from django.db import models

# Create your models here.
class Admision(models.Model):
	PROMEDIO= (
		('0', '0'),
		('1', '1'),
		('2', '2'),
		('3', '3'),
		('4', '4'),
		('5', '5'),
		('6', '6'),
		('7', '7'),
			)
	GRADO = (
		('Kinder','Kinder'),
		('Básica','Básica'),
		('Bachillerato','Bachillerato'),
		('Diversificado','Diversificado'),
		)
	grado = models.CharField(max_length=20, choices=GRADO, default= 'Kinder')		
	rut_representante = models.CharField(max_length=10)
	nombre_representante = models.CharField(max_length=15)
	apellido_representante = models.CharField(max_length=15)
	direccion = models.CharField(max_length=100)
	telefono = models.CharField(max_length=10)
	email = models.EmailField()
	rut_alumno = models.CharField(max_length=10)
	nombre_alumno = models.CharField(max_length=15)
	apellido_alumno = models.CharField(max_length=15)
	fechaNacimiento = models.DateField()
	promedio = models.CharField(max_length=2, choices=PROMEDIO, default= '0')
	def __str__(self):
		return '{} {} {} {}'.format(self.rut_alumno, self.nombre_alumno, self.promedio, self.grado)
