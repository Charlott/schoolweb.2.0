from django import forms
from apps.admision.models import Admision

class AdmisionForm(forms.ModelForm):

    class Meta:
        model = Admision

        fields = [
            'rut_representante',
            'nombre_representante',
            'apellido_representante',
            'direccion',
            'telefono',
            'email',
            'rut_alumno',
            'nombre_alumno',
            'apellido_alumno',
            'fechaNacimiento',
            'grado',
            'promedio',
        ]

        labels ={
            'rut_representante': 'Rut Representante',
            'nombre_representante': 'Nombre Representante',
            'apellido_representante': 'Apellido Representante',
            'direccion': 'Dirección',
            'telefono': 'Teléfono',
            'email': 'Email',
            'rut_alumno': 'Rut Alumno',
            'nombre_alumno': 'Nombre Alumno',
            'apellido_alumno': 'Apellido Alumno',
            'fechaNacimiento': 'Fecha Nacimiento',
            'grado': 'grado',
            'promedio': 'Promedio',
        }

        widgets = {
            'rut_representante': forms.TextInput(attrs={'class':'form-control'}),
            'nombre_representante': forms.TextInput(attrs={'class':'form-control'}),
            'apellido_representante': forms.TextInput(attrs={'class':'form-control'}),
            'direccion': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'email': forms.TextInput(attrs={'class':'form-control' ,'type':'email'}),
            'rut_alumno': forms.TextInput(attrs={'class':'form-control'}),
            'nombre_alumno': forms.TextInput(attrs={'class':'form-control'}),
            'apellido_alumno': forms.TextInput(attrs={'class':'form-control'}),
            'fechaNacimiento': forms.DateTimeInput(attrs={'class':'form-control datetimepicker-input', 'type':'date'}),
            'grado': forms.Select(attrs={'class':'form-control'}),
            'promedio': forms.Select(attrs={'class':'form-control'}),
        }