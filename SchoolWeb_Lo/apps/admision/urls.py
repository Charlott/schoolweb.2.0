from django.conf.urls import url
from apps.admision.views import admision, admisionCreate, admisionList

urlpatterns = [
    url(r'^$', admision, name='admision'),
    url(r'^nuevo$', admisionCreate.as_view(), name='admision'),
    url(r'^listar$', admisionList.as_view(), name='admision_listar'),    
]