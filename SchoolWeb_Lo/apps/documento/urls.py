from django.conf.urls import url, include
from apps.documento.views import documentoDelete, documentoList, documentoUpdate, documentoCreate
from django.contrib.auth.decorators import login_required


urlpatterns = [
url(r'^nuevo/$', login_required(documentoCreate.as_view()), name='documento'),
url(r'^listar/$', documentoList.as_view(), name='documento_listar'),
url(r'^editar/(?P<pk>\d+)/$', documentoUpdate.as_view(), name='documento_editar'),
url(r'^eliminar/(?P<pk>\d+)/$', documentoDelete.as_view(), name='documento_eliminar'),
]