# Generated by Django 2.2.5 on 2019-11-10 17:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('documento', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documento',
            name='tipoDocumento',
            field=models.CharField(choices=[('Notas Certificadas', 'Notas Certificadas'), ('Permiso Especial', 'Notas Certificadas'), ('Representante Adicional', 'Representante Adicional'), ('Certificación de Curso', 'Certificación de Curso')], default='Notas Certificadas', max_length=40),
        ),
    ]
