from django import forms
from apps.documento.models import Documento

class DocumentoForm(forms.ModelForm):

    class Meta: 
        model = Documento
        
        fields = [
            'fechaSolicitud',
            'rut',
            'tipoDocumento',
            'emision',
            'comentario', 
        ]

        labels ={
            'fechaSolicitud':'Fecha Solcitud',
            'rut': 'Rut Alumno',
            'tipoDocumento':'Documento',
            'emision': 'Emision',
            'comentario': 'Comentario',
        }

        widgets = {
            'fechaSolicitud': forms.DateTimeInput(attrs={'class':'form-control'}),
            'rut': forms.Select(attrs={'class':'form-control'}),
            'tipoDocumento': forms.Select(attrs={'class':'form-control'}),
            'emision': forms.Select(attrs={'class':'form-control'}),
            'comentario': forms.Textarea(attrs={'class':'form-control'}),
        }        