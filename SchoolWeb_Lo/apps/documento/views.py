from django.shortcuts import render, redirect
from django.http import HttpResponse
from apps.documento.forms import DocumentoForm
from apps.documento.models import Documento
from datetime import datetime
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
# Create your views here.


class documentoList (ListView):
    model = Documento
    template_name = 'documento/documento_listar.html'

class documentoCreate (CreateView):
    model = Documento
    form_class = DocumentoForm
    template_name = 'documento/documento.html'
    success_url = reverse_lazy('documento:documento_listar')
    
class documentoUpdate (UpdateView):
    model = Documento
    form_class = DocumentoForm
    template_name = 'documento/documento.html'
    success_url = reverse_lazy('documento:documento_listar')
    
class documentoDelete (DeleteView):
    model = Documento
    template_name = 'documento/documento_delete.html'
    success_url = reverse_lazy('documento:documento_listar')