from django.db import models
from datetime import datetime
from apps.alumno.models import Alumno

# Create your models here.
class Documento(models.Model):
    fechaSolicitud = models.DateTimeField(default=datetime.now, null=False, blank=False)
    rut = models.ForeignKey(Alumno, null=True, blank=False, on_delete=models.CASCADE)
    TIPODOCUMENTO = (
		('Notas Certificadas', 'Notas Certificadas'),
		('Permiso Especial', 'Permiso Especial'),
        ('Representante Adicional', 'Representante Adicional'),
        ('Certificación de Curso', 'Certificación de Curso'),
	) 
    tipoDocumento = models.CharField(max_length=40, choices=TIPODOCUMENTO, default='Notas Certificadas')
    EMISION =(
        ('Digital','Digital'),
        ('Físico','Físico'),
    )
    emision = models.CharField(max_length=15, choices=EMISION, default ='Digital')   
    comentario = models. CharField(max_length = 200, blank= True, null= True)

    def __str__(self):
     return '{}'.format(self.rut)