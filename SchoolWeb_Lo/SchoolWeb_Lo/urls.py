"""SchoolWeb_Lo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth.views import LoginView
from django.contrib.auth.views import LogoutView
from django.contrib.auth.views import logout_then_login
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('', include('core.urls')),
    url(r'^inscripcion/', include (('apps.inscripcion.urls', 'inscripcion'), namespace='inscripcion')),  
    url(r'^alumno/', include ('apps.alumno.urls')),
    url(r'^admision/', include ('apps.admision.urls')),
    url(r'^documento/', include(('apps.documento.urls', 'documento'), namespace='documento')),      
    url(r'^login/', LoginView.as_view(), name='login'),
    url(r'^accounts/login/', LoginView.as_view(), name='login'),
    url(r'^logout/', logout_then_login, name='logout'),    
]